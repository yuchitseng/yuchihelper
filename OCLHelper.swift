//
//  YuchiHelper.swift
//
//  Created by Yu Chi on 2016/8/2.
//  Copyright © 2016年 yuchi. All rights reserved.
//

import Foundation
import AVFoundation
import CoreLocation
import SystemConfiguration.CaptiveNetwork
import AssetsLibrary
import Photos
import UIKit

/**
 阿棋 幫手  v 1.0
 如何使用
 把這個swift 匯入 專案中就可以立即使用
 */
class OCLHelper {
    class Speech
    {
        /**
          給一段文字，透過系統播放語音
          */
        static func playVoice(_ string : String,speed:Float?,language:String?) {
            
            let utterance = AVSpeechUtterance(string: string)
            if speed != nil { utterance.rate = speed! }
            if language != nil { utterance.voice = AVSpeechSynthesisVoice(language: language) }
            let synth = AVSpeechSynthesizer()
            synth.speak(utterance) }
        
    }
    /**
     各種創建view 方法
     */
    class View {
        
        /**
         給一UIImage list 播放動畫
         */
        static func createLoadingViewAnimation(_ arrImage:[UIImage])->UIImageView {
            /*var array : [UIImage] = []
             for i in 0  ..< 19
             {
             array.append(UIImage(data: NSData(contentsOfFile: NSBundle.mainBundle().pathForResource("Connecting-\(i+1).png", ofType: "")!)!)!)
             }*/
            let frame = CGRect(x: 0, y: 0, width: 80, height: 80)
            let imageView = UIImageView(frame: frame)
            imageView.animationImages = arrImage
            imageView.animationDuration = 1;
            imageView.animationRepeatCount = 0
            return imageView }
        
        /**
         *創造一個loadingview
         */
        static func CreateLoadingView()->UIView {
            
            let loadingView:UIView = UIView()
            let screenSize : CGSize = UIScreen.main.bounds.size
            loadingView.bounds.size = screenSize
            loadingView.frame.origin = CGPoint(x: 0, y: 0)
            loadingView.backgroundColor = UIColor.black
            loadingView.alpha = 0.5
            let activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
            activityIndicator.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
            activityIndicator.center = loadingView.center
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
            loadingView.addSubview(activityIndicator)
            activityIndicator.startAnimating()
            return loadingView }
        /**
         創一個會顯示app版本的view
         */
        static func createVersionLabel(_ fontSize:CGFloat)->UIView
        {
            let str = Device.appVersion()
            let font:UIFont! = YuchiFont.fontWithStr(str, fontSize: fontSize)
            let attr : Dictionary<String,AnyObject> = [NSFontAttributeName : font]
            let size : CGSize = str.size(attributes: attr)
            let point : CGPoint = CGPoint(x: (UIScreen.main.bounds.width - size.width), y: UIScreen.main.bounds.height - size.height )
            let rect : CGRect = CGRect(x: point.x-15, y: point.y, width: size.width+30, height: size.height)
            let version = UILabel(frame: rect)
            version.text = str
            version.textAlignment = NSTextAlignment.center
            version.textColor = UIColor.white
            return version }

    }
    class Label {
        static func heightDynamic(_ wlabel:CGFloat,font:UIFont,text:String) -> CGFloat {
            
            let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: wlabel, height: CGFloat.greatestFiniteMagnitude))
            label.numberOfLines = 0
            label.lineBreakMode = NSLineBreakMode.byWordWrapping
            label.font = font
            label.text = text
            label.sizeToFit()
            return label.frame.height
            
        }

        /**
         讓一長串文字算出符合他的Size
         */
        static func setLabelSize(_ text:String,
                                fontSize:CGFloat,
                                sizeLimit:CGSize) -> CGRect {
            let str : NSString = text as NSString
            let font:UIFont! = YuchiFont.fontWithStr(str as String, fontSize: fontSize)
            let size : CGSize = sizeLimit//CGSize(width: self.textInputView.frame.width - 20, height: UIScreen.mainScreen().bounds.height)
            let point : CGPoint = CGPoint(x: 10, y: 0)
            var rect : CGRect = CGRect(origin: point, size: size)
            let pstyle = NSMutableParagraphStyle()
            pstyle.lineBreakMode = NSLineBreakMode.byWordWrapping
            rect = (str as NSString).boundingRect(with: rect.size, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName:font,NSParagraphStyleAttributeName:pstyle], context: nil)
            //rect.origin = CGPoint(x: 10 , y: (first_name_items_bg.frame.height - rect.size.height) / 2)
            //rect.size.width = self.textInputView.frame.width - 20

            return rect }
        
        /**
         創建畫面上的label 只要給point
         */
        static func createLabel(_ str:NSString,point:CGPoint,font:UIFont,textAlignment:NSTextAlignment?,textColor:UIColor?) -> UILabel {
            let attr : Dictionary<String,AnyObject>? = [NSFontAttributeName : font]
            let size : CGSize = str.size(attributes: attr)
            let rect : CGRect = CGRect(x: point.x, y: point.y, width: size.width, height: size.height)
            let label = UILabel(frame: rect)
            label.text = str as String
            if textColor != nil {
                label.textColor = textColor }
            label.font = font
            if textAlignment != nil {
                label.textAlignment = textAlignment! }
            
            return label }
        
        /**
         創建畫面上的label 只要給rect
         */
        static func createLabel(_ str:NSString,rect:CGRect,font:UIFont,textAlignment:NSTextAlignment?,textColor:UIColor?) -> UILabel {
            let label = UILabel(frame: rect)
            label.text = str as String
            if textColor != nil {
                label.textColor = textColor }
            label.font = font
            if textAlignment != nil {
                label.textAlignment = textAlignment! }
            
            return label }

    }
    class TextField {
        /**
         創建畫面上的輸入窗
         */
        static func createUITextField(_ str:NSString,
                                      rect:CGRect,
                                      delegate:UITextFieldDelegate,
                                      secureTextEntry:Bool,
                                      font:UIFont,
                                      textBorderStyle:UITextBorderStyle,
                                      keyBoardType:UIKeyboardType)->UITextField {
            let textField = UITextField(frame: rect)
            textField.font = font
            textField.textColor = UIColor.black
            textField.borderStyle = textBorderStyle
            textField.placeholder = str as String
            textField.delegate = delegate
            textField.isSecureTextEntry  = secureTextEntry
            textField.returnKeyType = UIReturnKeyType.done
            textField.keyboardType = keyBoardType
            return textField }
    }
    /**
     各種創建button 方法
     */
    class Button {
        /**
         創建一個基本按鈕  私有
         */
        fileprivate static func createButton(_ title:String,
                                         titleAlignment:UIControlContentHorizontalAlignment,
                                         rect:CGRect,
                                         font:UIFont,
                                         colorforNormal:UIColor,
                                         colorhighlighted:UIColor) -> UIButton {
            let button = UIButton();
            button.setTitle(title, for: UIControlState())
            button.setTitleColor(colorforNormal, for: UIControlState())
            button.setTitleColor(colorhighlighted, for: UIControlState.highlighted)
            button.backgroundColor =  UIColor.clear
            
            button.frame = rect
            button.titleLabel?.font = font
            button.contentHorizontalAlignment = titleAlignment
            return button }
        
        /**
         創建一個圓角有邊顏色顏色漸層的Button
         */
        static func createButton(_ title:String,
                                 titleAlignment:UIControlContentHorizontalAlignment,
                                 rect:CGRect,
                                 cornerRadius:CGFloat,
                                 borderWidth:CGFloat,
                                 font:UIFont,
                                 backColors:[CGColor],
                                 colorforNormal:UIColor,
                                 colorhighlighted:UIColor) ->UIButton {
            let button = createButton(title, titleAlignment: titleAlignment, rect: rect, font: font, colorforNormal: colorforNormal, colorhighlighted: colorhighlighted)
            let gradient = CAGradientLayer()
            gradient.frame = button.bounds
            gradient.colors = backColors
            gradient.cornerRadius = cornerRadius
            gradient.borderWidth = borderWidth
            let myColor : UIColor = UIColor( red: 0, green: 0, blue:0, alpha: 1.0 )
            gradient.borderColor = myColor.cgColor
            button.layer.insertSublayer(gradient, at: 0)
            
            return button }
        
        /**
         創建一個有底圖的Button
         */
        static func createButton(_ title:String,
                                 titleAlignment:UIControlContentHorizontalAlignment,
                                 rect:CGRect,
                                 cornerRadius:CGFloat,
                                 borderWidth:CGFloat,
                                 font:UIFont,
                                 backImage:UIImage,
                                 colorforNormal:UIColor,
                                 colorhighlighted:UIColor) ->UIButton {
            let button = createButton(title, titleAlignment: titleAlignment, rect: rect, font: font, colorforNormal: colorforNormal, colorhighlighted: colorhighlighted)
            
            button.titleLabel?.numberOfLines = -1
            button.titleLabel?.textAlignment = NSTextAlignment.center
            button.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            button.setBackgroundImage(backImage, for: UIControlState())
            
            button.layer.cornerRadius = cornerRadius
            button.layer.borderWidth = borderWidth
            button.layer.masksToBounds = true
            
            let myColor : UIColor = UIColor( red: 0, green: 0, blue:0, alpha: 1.0 )
            button.layer.borderColor = myColor.cgColor
            return button }
    }
    /**
     取得各種font
     */
    class YuchiFont {
        /**
         根據文字取得系統 font
         */
        static func fontWithStr(_ str:String,fontSize:CGFloat) ->UIFont {
            var font:UIFont!
            let fontSizeRate : CGFloat! = UIScreen.main.bounds.width / 320
            if OCLHelper.Device.isIpad() != true {
                font = UIFont.systemFont(ofSize: fontSize*fontSizeRate)}
            else {
                font = UIFont.systemFont(ofSize: (fontSize / 1.25)*fontSizeRate)}
            return font }
        
        /**
         根據文字取得 font sample : "Avenir Next Bold"
         */
        static func fontWithStr(_ str:String,fontSize:CGFloat,fontType:String) ->UIFont {
            var font:UIFont!
            let fontSizeRate : CGFloat! = UIScreen.main.bounds.width / 320
            if OCLHelper.Device.isIpad() != true {
                font = UIFont(name: fontType, size: fontSize * fontSizeRate)}
            else {
                font = UIFont.systemFont(ofSize: (fontSize / 1.25)*fontSizeRate)}
            return font }
    }
    
    /**
     取得各種device information
     */
    class Device {
        
        static func getdocumentsPath() -> String  {
            let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
            let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
            let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
            return paths[0]
        }

        /**
         自建一個系統相簿中的相簿
         */
        static func createAlbumInPhoneAlbum(_ albumName:String) {
            let assetsLibrary = ALAssetsLibrary()
            let groups = NSMutableArray()
            assetsLibrary.enumerateGroupsWithTypes(ALAssetsGroupAlbum, usingBlock: { (group, stop) -> Void in
                if (group != nil) {
                    groups.add(group) }
                else
                {
                    print("name : \(group)")
                    var haveHDRGroup = false;
                    for _gp in groups {
                        let gp = _gp as! ALAssetsGroup
                        let name : String = (gp.value(forProperty: ALAssetsGroupPropertyName) as? String)!
                        if name.isEqual(albumName as String) == true {
                            haveHDRGroup = true }}
                    if (haveHDRGroup != true) {
                            PHPhotoLibrary.shared().performChanges({ () -> Void in
                                PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: albumName as String)},
                                completionHandler: { (success, error) -> Void in
                                    if (!success) {
                                        print("add error") }})
                        haveHDRGroup = true }}}) { (error) -> Void in
            }
        }

        static func isLocationOpen()->Bool {
            //檢查定位有無開啟
            if ((CLLocationManager.authorizationStatus() == CLAuthorizationStatus.notDetermined) || (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied)){
                //定位沒有開啟
                return false}
            return true}
        
        /**
         取得目前是否是 模擬器
         */
        static func isSimulator() -> Bool {
            let model : NSString! = UIDevice.current.model as NSString
            if model.range(of: "Simulator").length == 0 {
                return false}
            return true}
        
        /**
         取得 device 是 iPad or iPhone
         */
        static func isIpad()->Bool {
            let model : NSString! = UIDevice.current.model as NSString
            if model.range(of: "iPhone").length > 0 {
                return false }
            else if model.range(of: "iPad").length > 0 {
                return true }
            return false }
        
        /**
         取得手機 型號
         */
        static func platform() -> String {
            var size : Int = 0
            sysctlbyname("hw.machine", nil, &size, nil, 0)
            var machine = [CChar](repeating: 0, count: Int(size))
            sysctlbyname("hw.machine", &machine, &size, nil, 0)
            return String(cString: machine)}
        
        /**
         取得os 版本
         */
        static func osVersion() -> String {
            let osVersion = UIDevice.current.systemVersion
            return osVersion}
        
        /**
         取得 app 版本  及  build 號碼
         */
        static func appVersion() -> String {
            let infoDict : NSDictionary = Bundle.main.infoDictionary! as NSDictionary
            let cfBundleShortVersionString = "CFBundleShortVersionString"
            let ver : NSString = infoDict.value(forKey: cfBundleShortVersionString) as! NSString
            let cfBundleVersion = "CFBundleVersion"
            let buildver : NSString = infoDict.value(forKey: cfBundleVersion) as! NSString
            let appVersion = "v\(ver) b\(buildver)"
            return appVersion}
    }
    /**
     各種取得json 轉換的 方法
     */
    class YuchiJson {
        /**
        把json型態的dictionary 轉成字串
         */
        static func convertDictionaryToString(_ data:Dictionary<String,AnyObject>) -> String? {
            do {
                let jsonDict = try JSONSerialization.data(withJSONObject: data, options: [])
                let theJSONText = NSString(data: jsonDict,
                                           encoding: String.Encoding.ascii.rawValue)
                return theJSONText as? String }
            catch {
                print("dictionary to string error")
                return nil }}
        
        /**
         把字串型的json 轉成 dictionary
         */
        static func convertStringToDictionary(_ text: String) -> [String:AnyObject]? {
            if let data = text.data(using: String.Encoding.utf8) {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    return json as? [String:AnyObject]}
                catch {
                    print("string to dictionary error")
                    return nil }}
            return nil }
        
        /**
         response nsdata json解析
         */
        static func JSON_Parse(_ data: Data) -> Dictionary<String, AnyObject>? {
            let jsonData: Data = data /* get your json data */
            do {
                let jsonDict = try JSONSerialization.jsonObject(with: jsonData, options: [])
                return jsonDict as? Dictionary<String, AnyObject>;}
            catch {
                print("nsdata to dictionary error")
                return nil }}
    }
    /**
     各種 系統 相關類別
     */
    class Sys {
        /**取得目前連線wifi 的 SSID*/
        static func currentWifiSSID() -> String {
            var ssid: String = "";
            if let interfaces = CNCopySupportedInterfaces() {
                for i: Int in 0  ..< CFArrayGetCount(interfaces) {
                    let interfaceName : UnsafeRawPointer = CFArrayGetValueAtIndex(interfaces, i)
                    let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                    let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString)
                    if unsafeInterfaceData != nil{
                        let interfaceData = unsafeInterfaceData! as NSDictionary!
                        ssid = interfaceData?["SSID"] as! String
                    }
                    else{
                        ssid = ""
                    }}}
            return ssid; }

        /**
         打開 iOS 系統設定頁面 iOS8 以後支援
         */
        @available(iOS 8.0, *)
        static func openSettings()
        {
            let url = URL(string: UIApplicationOpenSettingsURLString)
            UIApplication.shared.openURL(url!)
        }
        /**
         取得 device可用空間
         */
        static func freeSpaceInBytes() -> Int64? {
            /*You are running out of disk space on device.
             Free up some disk space on device.*/
            let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            if let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: documentDirectoryPath.last!) {
                
                if let freeSize = systemAttributes[FileAttributeKey.systemFreeSize] as? NSNumber {
                    print("freeSize = \(freeSize.int64Value)")
                    return freeSize.int64Value
                }
            }
            // something failed
            return nil
        }
        /**
         取得 device 全部空間
         */
        static func tatleSpaceInBytes() -> Int64?
        {
            let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            if let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: documentDirectoryPath.last!) {
                
                
                if let size = systemAttributes[FileAttributeKey.systemSize] as? NSNumber {
                    print( "tatlesize = \(size.int64Value)")
                    return size.int64Value
                }
            }
            // something failed
            return nil
            
        }
    }
    /**
     各種工具
     */
    class Utiltiy {

        /**
         email 驗證字串是否符合
         */
        static func isValidEmail(_ str:NSString)->Bool {
            let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluate(with: str) }
        
        /**
         password 判斷是否包含數字  英文大小寫
         */
        static func isValidPassword(_ str:NSString,lengthfrom:String,lengthto:String)->Bool {//8,16
            let passwordRegEx = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{\(lengthfrom),\(lengthto)}$"
            let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
            return passwordTest.evaluate(with: str) }
        
        /**
         password 英文大小寫 沒有一定要有英文及數字
         */
        static func isValidPasswordTwo(_ str:NSString,lengthfrom:String,lengthto:String)->Bool {//8,16
            let passwordRegEx = "^[0-9A-Za-z]{\(lengthfrom),\(lengthto)}$"
            let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
            return passwordTest.evaluate(with: str) }
        
        /**
         判斷只能有數字
         */
        static func isValidPhoneNumber(_ str:NSString,lengthfrom:String,lengthto:String)->Bool {//9,16
            let phoneNumberRegEx = "^[0-9]{\(lengthfrom),\(lengthto)}$"
            let phoneNumberTest = NSPredicate(format:"SELF MATCHES %@", phoneNumberRegEx)
            return phoneNumberTest.evaluate(with: str) }
        
        /**
         判斷名字長度
         */
        static func isValidName(_ str:NSString,lengthfrom:String,lengthto:String)->Bool {//2,16
            //let nameRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]{1,16}$"
            let nameRegEx = "^[0-9A-Za-z\\s]{\(lengthfrom),\(lengthto)}$"
            let nameTest = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
            return nameTest.evaluate(with: str) }
    }
    //MARK: - 文本 -
    class TextView {
        static func heightDynamic (_ textView: UITextView) -> UITextView {
            let fixedWidth = textView.frame.size.width
            textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = textView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            textView.frame = newFrame;
            return textView;
        }
        
    }
//MARK: - 一些簡易文本屬性的helper -
    class Attribute {
        fileprivate var targetString:String!
        fileprivate var dicAttribute:[String:AnyObject]? = [:]
        fileprivate var paragraphStyle:NSMutableParagraphStyle?
        fileprivate var attachment:NSTextAttachment?
        fileprivate var stAttributedLink:NSMutableAttributedString?
        init(string:String)
        {
            self.targetString = string
        }
        func setSystemFontSize(_ size:CGFloat) -> Attribute {
            dicAttribute![NSFontAttributeName] = UIFont.systemFont(ofSize: size)
            return self
        }
        func setColor(_ color:UIColor) -> Attribute {
            dicAttribute![NSForegroundColorAttributeName] = color
            return self
        }
        func setAlignment(_ alignment:NSTextAlignment) -> Attribute {
            if paragraphStyle == nil
            {
                paragraphStyle = NSMutableParagraphStyle()
            }
            paragraphStyle!.alignment = alignment
            dicAttribute![NSParagraphStyleAttributeName] = paragraphStyle
            return self
        }
        func setLinks(_ target:String?,url:String?) -> Attribute {
            if target != nil && url != nil {
                if stAttributedLink == nil {
                    stAttributedLink = NSMutableAttributedString(string: target!)
                }
                stAttributedLink!.addAttribute(NSLinkAttributeName, value: url!, range: (stAttributedLink!.string as NSString).range(of: target!))
            }
            return self
        }
        func setLineSpaces(_ lineSpace:CGFloat) -> Attribute {
            if paragraphStyle == nil
            {
                paragraphStyle = NSMutableParagraphStyle()
            }
            paragraphStyle!.lineSpacing = lineSpace
            dicAttribute![NSParagraphStyleAttributeName] = paragraphStyle
            return self
        }
        func setImage(_ wTextView:CGFloat,image:UIImage) -> Attribute {
            if attachment == nil {
                attachment = NSTextAttachment()
            }
            //插入圖片
            attachment!.image = image
            
            let oldWidth = attachment!.image!.size.width;
            
            let scaleFactor = oldWidth / (wTextView - 10); //for the padding inside the textView
            attachment!.image = UIImage(cgImage: attachment!.image!.cgImage!, scale: scaleFactor, orientation: .up)
            //這是讓圖取代某一文字的方式，實際運用研究中
            //        attributedString.replaceCharactersInRange(NSMakeRange(0, 1), withAttributedString: attrStringWithImage)
            
            return self
        }
        
        func toResult() -> NSAttributedString {
            let resultString = NSMutableAttributedString(string: targetString, attributes: dicAttribute)
            if attachment != nil
            {
                let attrStringWithImage = NSAttributedString(attachment: attachment!)
                resultString.append(attrStringWithImage)
                
            }
            if stAttributedLink != nil
            {
                //目標字串位置
                let range = (resultString.string as NSString).range(of: stAttributedLink!.string)
                //取代目標的字串
                resultString.replaceCharacters(in: range, with: stAttributedLink!)
            }
            return resultString
        }
        //TODO: - 這是設置超連結的顏色底線等，不負責超連結本身 -
        /**預設藍色 這是設置超連結的顏色底線等，不負責超連結本身*/
        static func setLinkAttributed() -> [String:AnyObject]
        {
            return Attribute.setLinkAttributed(UIColor.blue)
        }
        /**
         這是設置超連結的顏色底線等，不負責超連結本身
         - color:
         設置超連結文字顏色與底線的顏色
         */
        static func setLinkAttributed(_ color:UIColor) -> [String:AnyObject]
        {
            var linkAttribute:[String:AnyObject] = [:]
            linkAttribute[NSForegroundColorAttributeName] = UIColor.blue
            linkAttribute[NSUnderlineColorAttributeName] = UIColor.blue
            linkAttribute[NSUnderlineStyleAttributeName] = NSUnderlineStyle.styleSingle.rawValue as AnyObject?
            return linkAttribute
        }
    }
    //MARK: - VoiceOver -
    class VoiceOver {
        //TODO: VoiceOver的附加
        static func setVoiceOver<T: UIView>(_ target: T)
        {
            if target.speechString != nil && target.speechString != ""
            {
                target.accessibilityValue = target.speechString
                target.isAccessibilityElement = true
            }
            else
            {
                target.isAccessibilityElement = false
            }
            
        }
    }
    //MARK: - 簡易開啟 -
    class Applocation {
        /**直接撥打，不會通知 */
        static func callPhoneImmediately(_ stPhone:String)  {
            let url = URL(string: "tel://\(stPhone)")!
            if(UIApplication.shared.canOpenURL(url)){
                UIApplication.shared.openURL(url)
            }else
            {
                print("Call not available")
            }
        }
        /**撥打電話，會通知 */
        static func callPhone(_ stPhone:String) {
            let url = URL(string: "telprompt:\(stPhone)")!
            if(UIApplication.shared.canOpenURL(url)){
                UIApplication.shared.openURL(url)
            }else
            {
                print("Call not available")
            }
        }
        static func openUrl(_ stUrl:String?)
        {
            if stUrl == nil || stUrl == "" {return}
            let url = URL(string: stUrl!)!
            if(UIApplication.shared.canOpenURL(url))
            {
                UIApplication.shared.openURL(url)
            }else
            {
                print("http not available")
            }
        }
    }
    class SimpleNotification {
        //TODO: -本地推播
        static func addNotification(_ body:String,userInfo:[String:AnyObject]) {
            // 初始化一个通知
            let localNoti = UILocalNotification()
            // 通知內容
            let body = body
            localNoti.alertBody = body
            // 收到通知时播放的聲音，沒設定就是系統音
            localNoti.soundName = UILocalNotificationDefaultSoundName
            //待機界面的滑動動作提示
            localNoti.alertAction = "打開App"
            // 通知上绑定的其他信息
            localNoti.userInfo = userInfo
            // 添加至系統的通知列
            UIApplication.shared.presentLocalNotificationNow(localNoti)
        }
    }

}
