//
//  YuchiExtensions.swift
//
//  Created by Yu Chi on 2016/8/2.
//  Copyright © 2016年 yuchi. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var html2AttributedString:NSAttributedString {
        var result : NSAttributedString!
        do
        {
            try result = NSAttributedString(data: data(using: String.Encoding.utf8)!, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute:String.Encoding.utf8], documentAttributes: nil)
        }
        catch
        {
            print("html attributed error")
        }
        return result
    }
    
}
extension UIImage
{
    func imageByScalingToSize(_ targetSize:CGSize) ->UIImage
    {
        var newImage : UIImage!
        let sourceImage : UIImage! = self
        let imageSize : CGSize = sourceImage.size
        let width : CGFloat = imageSize.width
        let height : CGFloat = imageSize.height
        let targetWidth : CGFloat = targetSize.width
        let targetHeight : CGFloat = targetSize.height
        var scaleFactor : CGFloat = 0.0
        var scaleWidth : CGFloat = targetWidth
        var scaleHeight : CGFloat = targetHeight
        
        var thumbnailPoint : CGPoint = CGPoint(x: 0.0, y: 0.0)
        if (!imageSize.equalTo(targetSize))
        {
            let widthFactor : CGFloat = targetWidth / width
            let heightFactor : CGFloat = targetHeight / height
            if widthFactor < heightFactor
            {
                scaleFactor = widthFactor
            }
            else
            {
                scaleFactor = heightFactor
                scaleWidth  = width * scaleFactor
                scaleHeight = height * scaleFactor
                if (widthFactor < heightFactor) {
                    thumbnailPoint.y = (targetHeight - scaleHeight) * 0.5;
                } else if (widthFactor > heightFactor) {
                    thumbnailPoint.x = (targetWidth - scaleWidth) * 0.5;
                }
                
            }
        }
        UIGraphicsBeginImageContext(targetSize);
        
        var thumbnailRect : CGRect = CGRect.zero;
        thumbnailRect.origin = thumbnailPoint;
        thumbnailRect.size.width  = scaleWidth;
        thumbnailRect.size.height = scaleHeight;
        
        sourceImage.draw(in: thumbnailRect)
        newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        
        if(newImage == nil)
        {
            print("could not scale image")
        }
        
        
        return newImage
    }
    /**
     image等比例縮放
     */
    func scaleImage(_ image:UIImage,toScale:CGFloat) -> UIImage
    {
        var scaledImage:UIImage!
        
        UIGraphicsBeginImageContext(CGSize(width: image.size.width * toScale, height: image.size.height * toScale))
        image.draw(in: CGRect(x: 0, y: 0, width: image.size.width * toScale, height: image.size.height * toScale))
        scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
    /**
     image自定義長寬
     */
    func reSizeImage(_ image:UIImage,toSize:CGSize)->UIImage
    {
        var reSizeImage : UIImage!
        UIGraphicsBeginImageContext(CGSize(width: toSize.width, height: toSize.height))
        image.draw(in: CGRect(x: 0, y: 0, width: toSize.width, height: toSize.height))
        reSizeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return reSizeImage
    }
}
extension UIView {
    class func loadFromNibNamed(_ nibNamed: String, bundle : Bundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
}
extension UIColor {
    convenience init(hexaString:String) {
        //將＃號去除
        let newHexaString = hexaString.replacingOccurrences(of: "#", with: "")
        self.init(
            red:   CGFloat( strtoul( String(Array(newHexaString.characters)[0...1]), nil, 16) ) / 255.0,
            green: CGFloat( strtoul( String(Array(newHexaString.characters)[2...3]), nil, 16) ) / 255.0,
            blue:  CGFloat( strtoul( String(Array(newHexaString.characters)[4...5]), nil, 16) ) / 255.0, alpha: 1.0 )
    }
}
/*
 判斷點擊狀態使用
 因為點擊事件被uiscrollView攔截
 所以改寫scrollView的方法
 讓事件傳遞下去
 */
extension UIScrollView {
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesBegan(touches, with: event)
        
    }
    open override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesMoved(touches, with: event)
    }
    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.next?.touchesEnded(touches, with: event)
    }
    open override func touchesCancelled(_ touches: Set<UITouch>?, with event: UIEvent?) {
        self.next?.touchesCancelled(touches!, with: event)
    }
}
