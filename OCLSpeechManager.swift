//
//  YuchiSpeechManager.swift
//  thsr
//
//  Created by Yu Chi on 2016/8/5.
//  Copyright © 2016年 yuchi. All rights reserved.
//

import UIKit
import AVFoundation
/**
 如何使用這個管理器
 1. 必須實體化 這個manager
 2. exsmple
 let speech = OurLoveCitySpeechManager()
 func click_voice()
 {
 let view1 = UIView()
 view1.speechString = "大家好，這裡是泰安服務區\n我是語音小助理\n"
 let view2 = UIView()
 view2.speechString = "這是我說的第二段話\n希望大家可以跟我好好相處\n"
 let view3 = UIView()
 view3.speechString = "還有5條訊息等您。"
 speech.playVoice(strs: [view1.speechString!,view2.speechString!,view3.speechString!])
 }
 */
class OCLSpeechManager: NSObject,AVSpeechSynthesizerDelegate {
    static let shared = OCLSpeechManager()
    /**播放狀態**/
    var isPlayVoice : Bool = false
    /**語音播放api*/
    fileprivate var synth : AVSpeechSynthesizer!
    /**傳入要說話的view*/
    fileprivate var parantView : UIView!
    /**蓋在點擊的view上面*/
    fileprivate var maskBtn : UIButton!
    /**連續點的時候記得之前的view 一次清除*/
    fileprivate var maskBtns : [UIButton] = []
    /**dobule click 後要執行的 callback*/
    fileprivate var selector : (()->())!
    
    fileprivate var utterance : AVSpeechUtterance!
    /**準備播放的語音陣列*/
    fileprivate var arr_str : [String] = []
    /**準備播放帶有語音文字的陣列*/
    fileprivate var arr_view : [UIView] = []
    /**語音播放完要做什麼事情*/
    fileprivate var handle_complete : (()->Void)!
    
    //MARK: - 初始化 及 設定的地方 -
    override init() {
        super.init()
        synth = AVSpeechSynthesizer()
        synth.delegate = self
    }
//MARK: - 提供 End User使用的 methods -
    /**給一串字串陣列  給語音管理器 播放*/
    func playVoice(_ strs:[String])
    {
        playVoice(strs, speed: nil, lan: nil)
    }
    /**暫停語音系統*/
    func pauseVoice()
    {
        synth.stopSpeaking(at: AVSpeechBoundary.immediate)
    }
    /**暫停後  繼續播放*/
    func continueVoice()
    {
        synth.continueSpeaking()
    }
    /**把整段語音 取消 並釋放 相關設定*/
    func stopVoice()
    {
        isPlayVoice = false
        synth.stopSpeaking(at: AVSpeechBoundary.immediate)
        arr_str.removeAll()
        arr_view.removeAll()
        handle_complete = nil
        selector = nil
        removeMaskBtn()
    }
    /**
     給一串字串陣列 並設定說話速度 及語系   給語音管理器 播放
     */
    func playVoice(_ strs : [String],speed:Float?,lan:String?)
    {
        //TODO: 新增播放狀態
        self.isPlayVoice = true
        var finalStr = ""
        for _str in strs
        {
            finalStr += "\(_str)\n"
        }
        finalStr = finalStr.replacingOccurrences(of: "\n", with: "。")
        arr_str = MixAllStr(finalStr)
        
        print(arr_str)
        playVoice(arr_str.removeFirst(), speed: speed, lan: nil)
        handle_complete = { ()->() in
            if self.arr_str.count > 0
            {
                self.playVoice(self.arr_str.removeFirst(), speed: speed, lan: nil)
            }
            else
            {
                self.handle_complete = nil
                //TODO: 新增播放狀態
                self.isPlayVoice = false
            }
            
        }
    }
    /**
     給一串有帶 UIView.speechString 屬性的 views 陣列 給語音管理器 播放
     */
    func palyViewVoice(_ views:[UIView],speed:Float?,lan : String?)
    {
        var finalStr = ""
        for _view in views
        {
            finalStr += "\(_view.speechString!)\n"
        }
        finalStr = finalStr.replacingOccurrences(of: "\n", with: "。")
        
        arr_str = MixAllStr(finalStr)
        
        playVoice(arr_str.removeFirst(), speed: speed, lan: lan)
        
        handle_complete = { ()->() in
            if self.arr_str.count > 0
            {
                self.playVoice(self.arr_str.removeFirst(), speed: speed, lan: lan)
            }
            else
            {
                self.handle_complete = nil
            }
        }
    }
    //MARK: - 播放 -
    /**
     給一段文字，透過系統播放語音
     */
    fileprivate func playVoice(_ str : String,speed:Float?,lan:String?)
    {
        synth.stopSpeaking(at: AVSpeechBoundary.immediate)
        utterance = AVSpeechUtterance(string: str)
        if speed != nil { utterance.rate = speed! }
        if lan != nil { utterance.voice = AVSpeechSynthesisVoice(language: lan) }
        synth.speak(utterance)
    }
    /**主要是模仿 voice over 說語音會匡起來 並要點兩下才能使用*/
    fileprivate func playViewVoice(_ view:UIView,str : String,speed:Float?,lan:String?,selector:(()->())?)
    {
        synth.stopSpeaking(at: AVSpeechBoundary.immediate)
        utterance = AVSpeechUtterance(string: str)
        if speed != nil { utterance.rate = speed! }
        if lan != nil { utterance.voice = AVSpeechSynthesisVoice(language: lan) }
        parantView = view
        
        if maskBtn != nil
        {
            maskBtns.append(maskBtn)
        }
        self.selector = nil
        if selector != nil
        {
            self.selector = selector
        }
        synth.speak(utterance)
        
    }
    //MARK: - other func -
    fileprivate func MixAllStr(_ str:String)->[String]
    {
        let characterSet = NSMutableCharacterSet()
        
        characterSet.addCharacters(in: "。")
        characterSet.addCharacters(in: "，")
        
        return str.components(separatedBy: characterSet as CharacterSet)
    }
    fileprivate func addMaskBtn()
    {
        if parantView == nil
        {
            return
        }
        maskBtn = UIButton()
        maskBtn.frame.size = parantView.frame.size
        maskBtn.backgroundColor = UIColor.clear
        maskBtn.layer.borderColor = UIColor.blue.cgColor
        maskBtn.layer.borderWidth = 2
        maskBtn.layer.cornerRadius = parantView.layer.cornerRadius
        if selector != nil
        {
            maskBtn.addTarget(self, action: #selector(callback), for: UIControlEvents.touchUpInside)
            
        }
        parantView.addSubview(maskBtn)
        
    }
    func callback()
    {
        if self.selector != nil
        {
            synth.stopSpeaking(at: AVSpeechBoundary.immediate)
            removeMaskBtn()
            self.selector()
            selector = nil
        }
    }
    fileprivate func removeMaskBtn()
    {
        while maskBtns.count > 0 {
            maskBtns.first?.removeFromSuperview()
            maskBtns.removeFirst()
        }
        if maskBtn != nil
        {
            maskBtn.removeFromSuperview()
            maskBtn = nil
        }
    }
    //MARK: - 判斷 文字斷點停頓用的func -
    func checkPausePoint(_ synthesizer: AVSpeechSynthesizer,speechString:NSString , range : NSRange)
    {
        
        let str = speechString.substring(with: range)
        print("講到什麼字了 \(str)")
        if  str == "。" ||
            str == "."
        {
            
        }
        else if str == "，" ||
            str == ","
        {
            
        }
    }
    deinit
    {//TODO: 釋放記憶體
        synth = nil
        parantView = nil
        maskBtn = nil
        maskBtns.removeAll()
        selector = nil
        utterance = nil
        handle_complete = nil
        arr_view.removeAll()
        arr_str.removeAll()
    }
    //MARK: - AVspeechSynthesizer delegate methods -
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance) {
        print(characterRange.toRange())
        checkPausePoint(synthesizer,speechString: utterance.speechString as NSString, range: characterRange)
    }
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didStart utterance: AVSpeechUtterance) {
        print("didStartSpeechUtterance")
        addMaskBtn()
    }
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        print("didFinishSpeechUtterance")
        removeMaskBtn()
        if handle_complete != nil
        {
            handle_complete()
        }
    }
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didCancel utterance: AVSpeechUtterance)
    {
        print("didCancelSpeechUtterance")
        removeMaskBtn()
        if handle_complete != nil
        {
            handle_complete()
        }
    }
    //TODO: - 為了再播報的時候加上外框 -
    static func showBorder<T: UIView>(_ target: T?)
    {
        if target != nil
        {
            target!.layer.borderWidth = 2
            target!.layer.borderColor = UIColor.blue.cgColor
        }
        
    }
    static func hiddenBorder<T: UIView>(_ target: T?)
    {
        if target != nil
        {
            target!.layer.borderColor = UIColor.clear.cgColor
        }
        
    }
    
}
//MARK: - uiview 擴展  為了  方便使用 speech manager -
private var key: Void?
extension UIView {
    var speechString: String? {
        get {
            return objc_getAssociatedObject(self, &key) as? String
        }
        
        set {
            objc_setAssociatedObject(self,
                                     &key, newValue,
                                     .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
}
