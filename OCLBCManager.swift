//
//  MyBeaconManager.swift
//  servicearea
//
//  Created by yuchitseng on 2016/9/19.
//  Copyright © 2016年 servicearea. All rights reserved.
//

import UIKit

class OCLBCManager: NSObject,BeaconServiceDelegate
{
    static let sharedInstance = OCLBCManager()
    let beaconService = BeaconService.sharedInstance() as? BeaconService
    func initManager()
    {
        beaconService?.delegate = self
        
        
        let success = beaconService?.start(withLicenseKey: "3fb8e5754d3745bbb3c022e76fc8a94d625a2c0691144cc7a")
        
        if success == false
        {
            print("連線失敗")
        }
        beaconService?.backgroundTriggerEventNotification = true

    }
    func triggerBeaconEvent(_ eventType: Int, eventContent: String!) {
        print("triggerBeaconEvent")
        self.localPushMassage(msg: "triggerBeaconEvent")
    }
    func triggerPositionBeacon(_ info: [AnyHashable : Any]!) {
        print("triggerPositionBeacon")
        self.localPushMassage(msg: "triggerPositionBeacon")
        
    }
    func localPushMassage(msg:String)
    {
        let notifiction = UILocalNotification()
        
        notifiction.alertBody = "觸發測試,\(msg)"
        UIApplication.shared.presentLocalNotificationNow(notifiction)
        
    }
    
}
